<?php

/**
 * @file
 * Enables modules and site configuration for a im_base site installation.
 */

use Drupal\Core\Form\FormStateInterface;

function im_base_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {

  $form['site_information']['site_name']['#default_value'] = 'IM Drupal site';

  $form['site_information']['site_mail']['#default_value'] = 'dev@internetmarketologi.ru';
  $form['admin_account']['account']['mail']['#default_value'] = 'dev@internetmarketologi.ru';
  $form['admin_account']['account']['name']['#default_value'] = 'root';

  //dpm($form);

}
